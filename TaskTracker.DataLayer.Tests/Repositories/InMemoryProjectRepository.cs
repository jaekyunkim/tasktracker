﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DataLayer.Model;

namespace TaskTracker.DataLayer.Tests.Repositories
{
    public class InMemoryProjectRepository : TaskTracker.DataLayer.RepositoryBase<Project>
    {
        protected override Project[] Data
        {
            get
            {
                return new Project[]
                {
                    new Project()
                    {
                        Id = new Guid("044047cf-e789-44b4-b7b4-de1b6175861c"),
                        Name = "TimeTracker"
                    },
                    new Project()
                    {
                        Id = new Guid("ff31a302-2993-404f-b7ca-e34853b890b4"),
                        Name = "AppointmentScheduler"
                    }
                };
            }
        }

        public override void Delete(Guid id)
        {
            //Data.AsQueryable<Project>().Where<>
        }

        public override Project GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Insert(Project project)
        {
            Data.ToList<Project>().Add(project);
        }

        public override void Update(Project project)
        {
            throw new NotImplementedException();
        }
    }
}
