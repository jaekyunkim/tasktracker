﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DataLayer.Model;

namespace TaskTracker.DataLayer.Tests.Repositories
{
    class InMemoryTaskRepository : TaskTracker.DataLayer.RepositoryBase<Model.Task>
    {
        protected override Model.Task[] Data
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public override Model.Task GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Insert(Model.Task task)
        {
            throw new NotImplementedException();
        }

        public override void Update(Model.Task task)
        {
            throw new NotImplementedException();
        }
    }
}
