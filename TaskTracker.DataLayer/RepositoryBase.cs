﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DataLayer.Interfaces;

namespace TaskTracker.DataLayer
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        protected abstract T[] Data { get; }

        public abstract void Insert(T data);

        public abstract void Update(T dataToUpdate);

        public abstract void Delete(Guid id);

        public abstract T GetById(Guid id);

        public IQueryable<T> GetAll()
        {
            return Data.AsQueryable();
        }

        public IQueryable<T> Get(System.Linq.Expressions.Expression<Func<T, bool>> predicate)
        {
            return Data.AsQueryable().Where(predicate);
        }

        public T GetSingle(Expression<Func<T, bool>> whereCondition)
        {
            return Data.AsQueryable().Where(whereCondition).FirstOrDefault<T>();
        }
    }
}
