﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TaskTracker.DataLayer.Interfaces;
using TaskTracker.DataLayer.Model;

namespace TaskTracker.DataLayer
{
    public class ProjectRepository : RepositoryBase<Project>
    {
        protected override Project[] Data
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public override Project GetById(Guid id)
        {
            throw new NotImplementedException();
        }

        public override void Insert(Project project)
        {
            throw new NotImplementedException();
        }

        public override void Update(Project projectToUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
