﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.DataLayer.Model
{
    public class Task
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public double Hours { get; set; }

        public ProjectTaskMap ProjectTaskMap { get; set; }
    }
}
