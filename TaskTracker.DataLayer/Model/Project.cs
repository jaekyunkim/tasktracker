﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTracker.DataLayer.Model
{
    public class Project
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public ICollection<ProjectTaskMap> ProjectTaskMaps { get; set; }
    }
}
